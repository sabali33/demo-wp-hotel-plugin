<?php 
class Hotel_App{
    private static $_instance;
    public function __construct(){
        
    }
    public static function get_instance(){
        if(self::$_instance){
            return self::$_instance;
        }
        session_start();
        //var_dump('got here');
        //self::activated();
        $instance  = new self();
        register_activation_hook( WPHA_BASE, [ $instance, 'activated' ] );
		
        register_deactivation_hook( WPHA_BASE, [ $instance, 'deactivated' ] );
        add_action('wp_enqueue_scripts', [$instance, 'add_scripts'] );
        add_action('admin_menu', [$instance, 'add_settings_page'] );
        add_action('pre_get_posts', [$instance, 'get_hotel_auth_token']);
        add_action( 'admin_post_hotel_logout', [$instance, 'logout'] );
        //add_action('register_activation_hook', [$instance, 'activated'] );
        //add_action('register_deactivation_hook', [$instance, 'deactivated'] );
        //add_action('wp_enqueue_scripts', [$instance, 'add_scripts'], $priority, $accepted_args)
        add_shortcode('room_list', [$instance, 'display_room_list']);
        add_shortcode('reservation_page', [$instance, 'display_reservation_page']);
        self::$_instance = $instance;
        
        return self::$_instance;
    }
    public function activated(){
        $room_list_page_obj = get_page_by_path('hotel-list', OBJECT, 'page');
        $reservation_page_obj = get_page_by_path('reservation-page', OBJECT, 'page');
        $oauth_page_obj = get_page_by_path('oauth-page', OBJECT, 'page');
        
        if( $room_list_page && $reservation_page && $oauth_page_obj ){
            return;
        }
        
        $room_list_page = [
            'post_title' => __('Hotel List', 'hotel-app'),
            'post_name' => 'hotel-list',
            'post_content' => '[room_list]',
            'post_status' => 'publish',
            'post_type' => 'page'
        ];
        $room_reservation_page = [
            'post_title' => __('Reservation Page', 'hotel-app'),
            'post_name' => 'reservation-page',
            'post_content' => '[reservation_page]',
            'post_status' => 'publish',
            'post_type' => 'page'
        ];
        $auth_page_arr = [
            'post_title' => __('oAuth Page', 'hotel-app'),
            'post_name' => 'oauth-page',
            'post_content' => 'You are being authenticated',
            'post_status' => 'private',
            'post_type' => 'page'
        ];
        $room_list_page_id = !$room_list_page_obj ? wp_insert_post($room_list_page) : $room_list_page->ID;
        $room_reservation_page_id = !$reservation_page_obj ? wp_insert_post($room_reservation_page) : $reservation_page->ID;
        $oauth_page_id = !$oauth_page_obj ? wp_insert_post($auth_page_arr) : $oauth_page_obj->ID; 
        
        add_option('hotel-page-ids', [
            'room_list' => $room_list_page_id,
            'reservation' => $room_reservation_page_id,
            'oauth_page' => $oauth_page_id,
        ]);
        
    }
    public function logout(){
        if(!wp_verify_nonce($_GET['nonce'], 'hotel-app')){
            return;
        }
        delete_option( 'hotel_auth_access_token' );
        $redirect_url = add_query_arg( ['logout' => true, 'message' => 'logged out' ], admin_url('admin.php?page=hotel-app') );
        wp_safe_redirect( $redirect_url );
        exit;
    }
    public function deactivated(){
        $pages = get_option('hotel-page-ids');
        foreach($pages as $key => $page_id){
           wp_delete_post( $page_id, true ); 
        }
        delete_option('hotel-page-ids');
        
    }
    public function add_scripts(){
        wp_enqueue_style('hotel-app-css', HOTEL_BASE_URL .'/css/style.css', array(), '', 'all' );
        wp_enqueue_script( 'jquery-ui-datepicker' );
        wp_enqueue_script( 'jquery-datepicker' );
        wp_enqueue_script('hotel-app-js', HOTEL_BASE_URL .'/js/hotel-script.js', ['jquery'], '', true);

    }
    public function display_room_list(){
        ob_start();
        $room_list = $this->parse_data();
        
        include HOTEL_BASE . '/templates/room-list.php';
        return ob_get_clean();
    }
    public function get_rooms_data(){
        //delete_transient('hotel-rooms-data');
        if( $data = get_transient('hotel-rooms-data')){
            return $data;
        }
        $endpoints = [
            'rooms',
            'prices',
            'types',
            'bookings'
        ];
        $data = [];
        $token = get_option('hotel_auth_access_token');
        foreach($endpoints as $endpoint){
            $args = $endpoint == 'bookings' ? [
                'headers' => [
                    'Authorization' => "Bearer {$token->access_token}",
                    'Accept' => 'application/json; charset=utf-8'
                ]
            ] : [];
            $data[$endpoint] = $this->get_request($endpoint);
        }
        set_transient( 'hotel-rooms-data', $data, 60 * 60 );
        return $data;
        
    }
    
    public function get_request($endpoint, $options){
        $url = 'http://127.0.0.1:8000/api/'.$endpoint;
        $response = wp_remote_get( $url, $args );
       
        if( !is_wp_error($response)){
            $body = wp_remote_retrieve_body( $response );
        }
        return json_decode($body);
    }
    public function parse_data(){
        $data = $this->get_rooms_data();
        $rooms = [];
        foreach( $data['rooms'] as $room){
            $room_price = array_filter($data['prices'], function($price) use($room){
                return intval($price->room_id) === $room->id;
            });
            $is_booked = array_filter((array) $data['bookings']->data, function($booking) use($room){
                return intval($booking->room_id) === $room->id;
            });
            $room_type =  array_filter($data['types'], function($type) use ($room){
                return intval($type->room_id) === $room->id;
            });
            $room->price = current($room_price)->regular_price;
            $room->type = current($room_type)->name;
            $room->booked_on = array_map(function($booking){
                $start_date = new DateTime($booking->start_date);
                $end_date = new DateTime($booking->end_date);
                return sprintf( __('from<span class="booking-start"> %s </span> to <span class="booking-end">%s</span>', 'hotel-app'), $start_date->format('m-d-Y') , $end_date->format('m-d-Y'));
            },$is_booked);
            $rooms[] = $room;
        }
        return $rooms;
    }
    public static function get_page_ids(){
        $room_list_page_obj = get_page_by_path('hotel-list', OBJECT, 'page');
        $reservation_page_obj = get_page_by_path('reservation-page', OBJECT, 'page');
        $oauth_page_obj = get_page_by_path('oauth-page');
        return [ 
            'room_list' => $room_list_page_obj->ID, 
            'reservation_page' => $reservation_page_obj->ID, 
            'oauth_page' => $oauth_page_obj->ID,
        ];
    }
    public function add_settings_page(){
        // add_submenu_page( 'options-general', __('Hotel Rooms Settings', 'hotel-app'), __('Hotel Rooms Settings', 'hotel-app'), 'manage_options', , [$this, 'display_hotel_settings'] );
        add_menu_page( __('Hotel Rooms Settings', 'hotel-app'), __('Hotel Rooms Settings', 'hotel-app'), 'manage_options', 'hotel-app', [$this, 'display_hotel_settings'] );
        // register_setting('hotel-app-section', 'hotel_app_option');

        // add_settings_section('hotel_app_sec', __('Hotel Rooms Settings', 'hotel-app'), [$this, 'display_hotel_settings'], 'hotel-app-section' );
    }
    public function display_hotel_settings(){
        include HOTEL_BASE . '/templates/settings.php';
    }
    public static function get_hotel_auth_token(){
        $pages = self::get_page_ids();
        if(!is_page($pages['oauth_page'])){
           
            return;
        }
       
        if(!$_GET['code']){
            return;
        }
        $args = [
            'grant_type' => 'authorization_code',
            'client_id' => '3',
            'client_secret' => 'Og7bvxZjFg83OZxOQnaITBQZ5u8KAmlLUPV6MJnF',
            'redirect_uri' => 'http://localhost:8888/dev/testing-site/great/',
            'code' => $_GET['code'],
        ]; 
        
        $response = wp_remote_post( 'http://127.0.0.1:8000/oauth/token', [
            'body' => $args, 
            'method' => 'POST',
            'headers' => [
                'Accept' => 'application/json; charset=utf-8'
            ]
            ] );
       
        if( is_wp_error($response)){
           
            return $response;
        }
        $resp = json_decode(wp_remote_retrieve_body($response));
        update_option( 'hotel_auth_access_token', $resp );
        $redirect_url = add_query_arg( ['signin' => true, 'message' => 'logged in'], admin_url('admin.php?page=hotel-app') );
        wp_safe_redirect( $redirect_url );
        exit();

    }
    public function display_reservation_page(){
        ob_start();
        $room_id = $_GET['room_id'];
        $data = $this->parse_data();
        
        $found_room = array_filter($data, function($room) use($room_id){
            return $room->id === intval($room_id);
        });

        $room = current($found_room);
        include HOTEL_BASE . '/templates/reservation-page.php';
       
        return ob_get_clean();
    }
}

Hotel_App::get_instance();