<div class="settings-container">
    <h1>Please login to Hotel App tp get access token</h1>
    <?php
    $args = [
        
        'client_id' => '3',
        'redirect_uri' => 'http://localhost:8888/dev/testing-site/oauth-page/',
        'response_type' => 'code',
        'scope' => '',
    ];
        $url = add_query_arg( $args, 'http://127.0.0.1:8000/oauth/authorize' );
    //delete_option( 'hotel_auth_access_token' );
    ?>
    <?php if( !get_option('hotel_auth_access_token')): ?>
    <a href="<?php echo esc_url($url); ?>"> Sign in</a>
    <?php else: ?>
    <?php
        $logout_url = add_query_arg( ['logout' => true, 'action' =>'hotel_logout', 'nonce'=> wp_create_nonce( 'hotel-app' )], admin_url('admin-post.php') );
    ?>
        <a href="<?php echo esc_url($logout_url); ?>"> <?php echo _e('Logout', 'hotel-app'); ?></a>
    <?php endif; ?>
</div>
<?php  
$token = json_encode( get_option('hotel_auth_access_token'));
var_dump($_SESSION);
$g =  [
    'headers' => [
        'Authorization' => "Bearer {$token}",
        'Accept' => 'application/json'
    ]
    ];
(new Hotel_App)->get_request('bookings', $g); ?>
