<?php
 $pages = Hotel_App::get_page_ids();
 //var_dump(Hotel_App::get_page_ids());
foreach( (array) $room_list as $room ){
    if(!$room->price){ continue; }

    ?>
    <div class="room-item">
        <div class="feature">
        <img src="<?php echo esc_url($room->room_image) ?>" alt="<?php echo esc_attr($room->name) ?>"></div>
        <div class="feature">
            <span class="label">
                <?php echo _e('Name:', 'hotel-app'); ?>
            </span>
            <span class="value">
                <?php echo esc_html($room->name); ?>
            </span>
            </div>
        
        <div class="feature">
            <span class="label">
                <?php echo _e('Price:', 'hotel-app'); ?>
            </span>
            <span class="value">
                <?php echo esc_html($room->price); ?>
            </span>
            
        </div>
        
        <div class="feature">
            <span class="label">
                <?php echo _e('Room Type:', 'hotel-app'); ?>
            </span> 
            <span class="value">
                <?php echo $room->type ? esc_html($room->type) : 'Basic'; ?>
            </span>
        </div>
        <div class="feature ">
        <?php
       
        $reservation_page = get_permalink($pages['reservation_page']);
        $url = add_query_arg( array( 'room_id' => $room->id), $reservation_page );
        if(!empty($room->booked_on)){
            echo implode('<br>', $room->booked_on);
        }else{
            echo _e('Available', 'hotel-app');
            echo '<span class="book-button-box">
            <a href="'.$url.'" class="button button-primary">Book</a>
            </span>';
        }
        ?>
        
         
        
        </div>
    </div>
    <?php
}