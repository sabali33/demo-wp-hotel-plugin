<div class="reservation-page">
    <div class="room-item">
        <div class="feature">
            <img src="<?php echo esc_url($room->room_image) ?>" alt="<?php echo esc_attr($room->name) ?>">
        </div>
        <div class="feature">
            <span class="label">
                <?php echo _e('Name:', 'hotel-app'); ?>
            </span>
            <span class="value">
                <?php echo esc_html($room->name); ?>
            </span>
            </div>
        
        <div class="feature">
            <span class="label">
                <?php echo _e('Price:', 'hotel-app'); ?>
            </span>
            <span class="value">
                <?php echo esc_html($room->price); ?>
            </span>
            
        </div>
        
        <div class="feature">
            <span class="label">
                <?php echo _e('Room Type:', 'hotel-app'); ?>
            </span> 
            <span class="value">
                <?php echo $room->type ? esc_html($room->type) : 'Basic'; ?>
            </span>
        </div>
    </div>
    
    <form action="<?php ?>" method="post">
    
        <div class="dates">
        <h2>Choose a date</h2>
            <div class="start-date">
                <input type="date" name="start_date" id="start-date" class="date-picker">
                <label for="start-date">Starts</label>
            </div>
            <span class="sep">to</span>
            <div class="end-date">
                <input type="date" name="end_date" id="end-date" class="date-picker">
                <label for="end-date">Ends </label>
            </div>
        </div>
    
        <div class="accounts-table">
            <div class="">
                <span class="label">Nights:</span>
                <span class="value nights" data-nights >1</span>
            </div>
            <div class="">
            <span class="label">Price:</span><span class="value" data-price="<?php echo esc_attr($room->price);?>"><?php echo esc_html($room->price);?></span>
            </div>
            <div class="">
            <span class="label">Amount:</span><span class="value" data-amount></span>
            </div>
        </div>
        <div class="customer-table">
            <p>
                <label for="full-name">Full Name</label>
                <input type="text" name="full_name" id="full-name">
            </p>
            <p>
                <label for="phone">Phone</label>
                <input type="tel" name="phone" id='phone'></p>
            <p>
                <label for="email">Email</label>
                <input type="email" name="email" id="email" ></p>
            <p>
                <input type="submit" value="<?php echo _e('Confirm Booking', 'hotel-app'); ?>">
            </p>
        </div>
    </form>
</div>
