"use strict";
(function($){
    $(document).ready(function(){
        $('.date-picker').datepicker({
            dateFormat : 'm/d/yy',
			numberOfMonths: 2,
        });
        var price  = $('[data-price]').data('price');
        var days = 1;
        var amount = price * days;
            $('[data-amount]').html(amount);
        $('.date-picker').change(function(){
            var values = $('.date-picker').serializeArray().map(function(value){
                return value.value;
            });
            days = findNumberOfDays(values[0], values[1]);
            $('[data-nights]').html(days || 1);
            amount = days * price;
            $('[data-amount]').html(Math.round(amount));
        })
    })
    function findNumberOfDays(start, end){
        if( !start || !end ){
            return null;
        }
        
        var startDate =  Date.parse(start);
        var endDate = Date.parse(end);
        var diffInSec = (endDate - startDate)/1000;
        var diffInDays = ((diffInSec)/60/60)/24;
        console.log(diffInDays);
        return diffInDays;
    }
})(jQuery)