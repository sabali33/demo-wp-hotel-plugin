<?php
/**
 * Plugin Name:     Hotel App
 * Plugin URI:      http://swimghana.com
 * Description:     A simple plugin to interface a demo hotel app
 * Author:          Eliasu Abraman
 * Author URI:      http://swimghana.com/eliasu
 * Text Domain:     hotel-app
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Hotel_App
 */

// Your code starts here.
define('HOTEL_BASE', plugin_dir_path( __FILE__ ) );
define('HOTEL_BASE_URL', plugin_dir_url( __FILE__ ));
define( 'WPHA_BASE', __FILE__ );

require_once( HOTEL_BASE . '/inc/init.php');