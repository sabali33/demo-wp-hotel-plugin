# Hotel App #
**Contributors:** (this should be a list of wordpress.org userid's)  
**Donate link:** https://example.com/  
**Tags:** comments, spam  
**Requires at least:** 4.5  
**Tested up to:** 5.2.4  
**Stable tag:** 0.1.0  
**License:** GPLv2 or later  
**License URI:** https://www.gnu.org/licenses/gpl-2.0.html  

## Description ##

A simple plugins that interfaces Hotel api for WordPress sites.

## Installation ##

This section describes how to install the plugin and get it working.

e.g.

1. Upload `Hotel-app` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to `/room-list` of your site address to find room list
4. Click to book a room

## Changelog ##

### 1.0 ###
* A change since the previous version.
* Another change.

